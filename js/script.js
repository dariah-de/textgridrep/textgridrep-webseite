/* 
   Scripts for TextGridRep Search Frontend
   Author: Patrick Heck (patrick@patrickheck.de)
   written in 2011
*/
$(function() {
	/* activate checktree functionality */
	$("ul.checkedtree").checkTree({ 
		onHalfCheck : function(elem) {
			elem.find("input.cParent").attr("value", "some");
		},
		onCheck : function(elem) {
			elem.find("input.cParent").attr("value", "all");
		},
		onUnCheck : function(elem) {
			elem.find("input.cParent").attr("value", "none");
		}
	});
	 
	/* add double arrows to box headers */
	$('.expandable-group h3').append('<div class="double-arrow" title="collapse/expand box"></div>');
	
	/* Expand/collapse on click on double arrows*/ 
	$(".expandable-group h3 .double-arrow").click(function(){
		window.log("trying to toggle checkbox");
		var thiseg = $(this).closest(".expandable-group");
		if (thiseg.hasClass("collapsed")) {
        //do the stuff that you would do when 'checked'
			thiseg.find(".expandable-content")
				.slideDown(200)
				.parent()
				.removeClass("collapsed");
			thiseg.find("input:checkbox:first").attr("checked",true);
		} else {
			thiseg.find(".expandable-content")
				.slideUp(200)
				.parent()
				.addClass("collapsed");
			thiseg.find("input:checkbox:first").removeAttr("checked");
				
		}
	});
	
	/* Set correct initial state in case checkboxes are initially unchecked */
	$(".expandable-group h3 input:checkbox").each(function(){
		 if (!$(this).attr("checked")) {
			$(this).closest(".expandable-group")
				.addClass("collapsed"); 
	     }
	});
	
	/* If checkbox is unchecked also collapse the box */
	$(".expandable-group h3 input:checkbox").change(function(){
		$(this).closest(".expandable-group").find(".double-arrow").click();
	});
	
	
	/* Functionality for the dynamically growing fields */
	/* add plus/minus buttons */
	$('.auto-grow-group li').append('<div class="plus" title="add another field after this one" ></div> <div class="minus" title="remove this field" style="display:none;"></div>');
	/* remove the surrounding <li> if minus button is clicked */
	$(".minus").live("click",function() {
		var thisli = $(this).closest("li");
		if (thisli.siblings().size() === 1) {
			thisli.siblings().find(".minus").hide();
		}
		/* plain removal for ie7+8 and fancy slideup for others */
		if (($("html").hasClass('ie7') || $("html").hasClass('ie8')) ) {
			thisli.remove();
		} else {
			thisli.slideUp(200).delay(200).queue(function() { $(this).remove(); });
		}
		
	});
	/* if the "plus" button is clicked clone this li and add it below this one */
	$(".plus").live("click",function() {
		var thisli = $(this).closest("li");
		var thisclone = thisli.clone();
		var highestindex = 0;
		var counter;
		
		/* determin the highest field index in use */
		thisli.parent().find("select").each(function () {
			 var thismatch = $(this).attr("name").match(/^metadata(\[([0-9]+)\])/);
			 var index = parseInt(thismatch[2],10);
			 
			 if (index > highestindex) {
				highestindex = index;
			 } 
		});
		counter = highestindex+1;
		
		thisclone.html(thisclone.html()).hide();
		thisli.after(thisclone);
		thisclone.find("input, select").each(function() {
			var newname =  $(this).attr("name").replace(/^metadata(\[[0-9]+\])/,"metadata["+counter+"]");
			$(this).attr("name",newname);
		});
		
		if (($("html").hasClass('ie7') || $("html").hasClass('ie8')) ) {
			thisclone.show();
		} else {
			thisclone.slideDown();
		}
		if (thisli.siblings().size() >= 1) {
			thisli.parent().find(".minus").show();
		}
		thisclone.find('input:text').quickClear();
	});
	
	/* automatic adding of table rows
	   if a key is pressed in the last field of the table another row is added
	*/
	$("table tr:last input").live("keypress",function() {
		var thistr = $(this).closest("tr");
		var thisclone = thistr.clone();
		var counter;
		var highestindex = 0;
		
		/* determin the highest field index in use */
		thistr.parent().find("input").each(function () {
			 var thismatch = $(this).attr("name").match(/(\[([0-9]+)\])$/);
			 var index = parseInt(thismatch[2],10);
			 
			 if (index > highestindex) {
				highestindex = index;
			 } 
		});
		counter = highestindex+1;
		
		thisclone.html(thisclone.html());
		thistr.after(thisclone);
		thisclone.find("input, select").each(function() {
			var newname =  $(this).attr("name").replace(/(\[[0-9]+\])$/,"["+counter+"]");
			$(this).attr("name",newname);
		});
	});
	
	/* trigger form submission if this element is clicked */
	$(".submit-button").click(function () {
		$(this).closest("form").submit();	
	});
	
	
	/* give all inputs clear buttons */
	$('input:text').each(function(){
		if ($(this).closest("table").size() === 0 ) {
			$(this).quickClear();
		}
	});
	
	/* a radiobutton dis-/enables all inputs that share the same <li> */
	$('input:radio').click(function() {
		$(this).closest("li").siblings().find("input:text").attr("disabled",true);
		$(this).closest("li").find("input:text").removeAttr("disabled").focus();
		
	});
	
	/* if a history line is double clicked - copy to query field */
	$("#history-select").dblclick(function() {
		$("#xpath-query").val($(this).find("option:selected").val());
	});
	
	/* so the same when the button is clicked */
	$("#insert-selected-btn").click(function() {
		$("#xpath-query").val($("#history-select option:selected").val());
	});
	
	/* remove all history entries */
	$("#clear-history-btn").click(function() {
		$("#history-select option").remove();
		// execute some ajax code
	});
	
	$("#browsing-filters input:checkbox").change(function(){
		/* add dynamic filter and change tree using ajax */
	});
	
	/* add elements to show KWIC and a link to add to "merkliste" */
	$(".resultset .kwic").after('<div class="small-arrow empty" title="show excerpt"></div>');
	$(".resultset li").append('<a class="merkliste" href="#">auf die Merkliste</a>');
	/* show KWIC if the small arrow is clicked */
	$(".resultset .small-arrow").live("click",function () {
		$(this).toggleClass("expanded").siblings(".kwic").slideToggle(150);	
	});
	
	/* if the "sorty by" select field is changed */
	$('#search-resultlist-order').change(function(){
		/* reload results using ajax */
	});
	
	/* if merkliste button is clicked */
	$("a.merkliste").click(function(){
		/* add this element to merkliste */
		$(this).toggleClass("added");
		if ($(this).hasClass("added")) {
			$(this).html("updating...");
			/* execute some ajax
			   the following code should be called upon completion 
			    */
			$('#merkliste-indicator .counter').html(parseInt($('#merkliste-indicator .counter').html(),10) +1 );
			$(this).html("von Merkliste entfernen")
		} else {
			$(this).html("updating...");
			/* execute some ajax
			   the following code should be called upon completion
			    */
			$('#merkliste-indicator .counter').html(parseInt($('#merkliste-indicator .counter').html(),10) -1 );
			$(this).html("auf die Merkliste");
		}
		return false;	
	});
	
	/* if an item from the refine search list is clicked */
	$("#refine-search a").click(function(){
		/* reload result list using ajax
			detailed parameters may be given through the href attribute
		*/
		return false;
	});
	
//	/* if the button to save all results is clicked */
//	$(".save-all-results").click(function(){
//		/* execute some ajax */
//		return false;	
//	});
	
});

$(document).ready(function() {
  $.get(statusURL, function(data, textStatus, request) {
    var statusHtml = $(data).find('.repstatus').not('.ok'),
	container = $("#main");
    container.prepend(statusHtml);  
  }, "html")
});
