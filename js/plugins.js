
// remap jQuery to $
(function($){
/**
    Project: CheckTree jQuery Plugin
    Version: 0.3 (porkchop)
    Project Website: http://jquery-checktree.googlecode.com/
    Author: JJ Geewax <jj@geewax.org>
    
    License:
        The CheckTree jQuery plugin is currently available for use in all personal or 
        commercial projects under both MIT and GPL licenses. This means that you can choose 
        the license that best suits your project and use it accordingly.
*/
(function(jQuery) {
jQuery.fn.checkTree = function(settings) {
    
    settings = jQuery.extend({
        /* Callbacks
            The callbacks should be functions that take one argument. The checkbox tree
            will return the jQuery wrapped LI element of the item that was checked/expanded.
        */
        onExpand: null,
        onCollapse: null,
        onCheck: null,
        onUnCheck: null,
        onHalfCheck: null,
        onLabelHoverOver: null,
        onLabelHoverOut: null,
        
        /* Valid choices: 'expand', 'check' */
        labelAction: "check",
        
        // Debug (currently does nothing)
        debug: false
    }, settings);
    
    var $tree = jQuery("ul.checkedtree");
	var $lis = $tree.find('li');
	var $checkboxes = $lis.find(":checkbox");
	// var $checkboxes = $(":checkbox");
	
	// Hide all checkbox inputs
    $checkboxes.css('display', 'none');
	$checkboxes.before('<div class="checkbox"></div>');
	
	$lis.not(':has(.arrow)').each(function() {
		// This little piece here is by far the slowest.
		jQuery(this).prepend('<div class="arrow"></div>');
	});
	
	// Hide all sub-trees
	$tree.find('li > ul').css('display', 'none');
	
	/*
	What to do when the arrow is clicked
	Tried:
	 - $lis.filter(':has(li)').find(' > .arrow')
	 - $lis.filter(':has(li)').find('.arrow')
	 - $tree.find('li:has(li) .arrow')
	 - $tree.find('li:has(li) > .arrow') <- This was the fastest.
	*/
	$('li:has(li) > .arrow')
		.click(function() {
			var $this = jQuery(this);
			$this
				.toggleClass('expanded')
				.siblings("ul:first")
					.slideToggle(200)
			;
			
			// Handle callbacks
			if (settings.onExpand && $this.hasClass('expanded')) {
				settings.onExpand($this.parent());
			}
			else if (settings.onCollapse){
				settings.onCollapse($this.parent());
			}
		})
		.addClass("visible")
		;
	
	/*
	What to do when the checkbox is clicked
	*/
	$tree.find('.checkbox').click(function() {
		var $this = jQuery(this);
		$this
			.toggleClass('checked')
			.removeClass('half_checked')
			.siblings(':checkbox:first').attr('checked', $this.hasClass('checked') ? 'checked' : '')
		;
		
		$this.filter('.checked').siblings('ul:first').find('.checkbox:not(.checked)')
			.removeClass('half_checked')
			.addClass('checked')
			.siblings(':checkbox').attr('checked', 'checked')
		;
		$this.filter(':not(.checked)').siblings('ul:first').find('.checkbox.checked')
			.removeClass('checked half_checked')
			.siblings(':checkbox').attr('checked', '')
		;
		
		// Send a change event to our parent checkbox:
		$this.parents("ul:first").siblings(":checkbox:first").change();
		
		// Handle callbacks
		if (settings.onCheck && $this.hasClass('checked')) {
			settings.onCheck($this.parent());
		}
		else if (settings.onUnCheck && $this.hasClass('checked') == false) {
			settings.onUnCheck($this.parent());
		}
	});
	
	/*
	What to do when a checkbox gets a change event
	(Fired when the children of this checkbox have changed)
	*/
	$checkboxes.change(function() {
		// If all the children are checked, this should be checked:
		var $this = jQuery(this);
		var $checkbox = $this.siblings('.checkbox:first');
		var any_checked = $this.siblings('ul:first').find(':checkbox:checked:first').length == 1;
		var any_unchecked = $this.siblings('ul:first').find(':checkbox:not(:checked):first').length == 1;
		
		if (any_checked) {
			$this.attr('checked', 'checked');
			if (any_unchecked) {
				$checkbox
					.addClass('half_checked')
					.removeClass('checked')
				;
				if (settings.onHalfCheck) {
					settings.onHalfCheck($this.parent());
				}
			}
			else {
				$checkbox
					.addClass('checked')
					.removeClass('half_checked')
				;
			}
		}
		else {
			$checkbox.removeClass('checked half_checked');
			$this.attr('checked', '');
		}
	});
	
	/*
	What to do when a label is hovered or clicked
	*/
	$tree.find('label')
		.click(function() {
			switch(settings.labelAction) {
				case 'expand':
					jQuery(this).siblings('.arrow:first').click();
					break;
				case 'check':
					jQuery(this).siblings('.checkbox:first').click();
					break;
			}
		})
		
		.hover(
			function() {
				// jQuery(this).addClass("hover");
				if (settings.onLabelHoverOver) {
					// jQuery(this).siblings('.checkbox:first').mouseover();
					settings.onLabelHoverOver(jQuery(this).parent());
				}
			},
			function() {
				// jQuery(this).removeClass("hover");
				if (settings.onLabelHoverOut) {
					// jQuery(this).siblings('.checkbox:first').mouseout();
					settings.onLabelHoverOut(jQuery(this).parent());
				}
			}
		)
	;
	
	/*
	Extra convenience methods
	*/
	$tree.clear = function() {
		$tree.find('.checkbox')
			.removeClass('checked')
			.siblings(':checkbox').attr('checked', '')
		;
	};
	

	
	// need checked trees
	$tree.find('.checkbox')
			.addClass('checked')
			.siblings(':checkbox').attr('checked', 'true')
	
};





})(jQuery);
 

/*! 
 * Copyright (c) 2010 Scott Murphy @hellocreation (http://scott-murphy.net)
 * 
 * jQuery quickClear plugin
 * Version 1.0 (Aug 2010)
 * @requires jQuery v1.2.3 or later
 *
 * Licensed under the MIT License
 */
(function($){
	$.fn.quickClear = function(options) {

		var defaults = {
			clearImg : "<span  />",
			container: "<span class=\"clearBtnContainer\"></span>",
			padding : 5
		};
	
		var options = $.extend(defaults, options);

		return this.each(function() {	

			var textField = $(this); 
			var clearButton = $(options.clearImg);
			var container = $(options.container);
			var clicked = false;  //flag when button is pressed, prevent blur from firing.
			
			init();

			function init() {
				createClearButton();
				clearButton = textField.siblings('.clearButton')	
				container = textField.parent('.clearBtnContainer');	
				textField.bind({
					  focus: function() {
							showClearButton();
					  },
					  focusout: function() {
						    if (clicked) { 
								return clicked = false;
							} else {
								removeClearButton();
							};
					  }
				});
				clearButton.bind({
						mousedown: function() {
							clicked = true; 
							clearValue();
							//setTimeOut to allow focus event to fire after the blur event
							setTimeout(function() { textField.focus();}, 0);  	
						}
				});
				textField.parent("span").bind({
						mouseover: function() {
							showClearButton();
						},
						mouseout: function() {
							removeClearButton();	
						}
				});
			}
					
			function createClearButton() {
				if (! textField.parent(".clearBtnContainer").size() ) {
					textField.wrap(container).after(clearButton);
					clearButton.hide().addClass('clearButton');
					
					//adjust width of input field based on image size
					var clearBtnWidth = clearButton.outerWidth();
					textField.css('padding-right', clearBtnWidth + options.padding);
					textField.width(textField.width() - clearBtnWidth - options.padding);
				}
			}
			
			function showClearButton() { clearButton.show(); }
			function removeClearButton() {  clearButton.hide(); }
			function clearValue() { textField.val(""); }
			
		});
	};

})(jQuery);


})(this.jQuery);



// usage: log('inside coolFunc',this,arguments);
// paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function(){
  log.history = log.history || [];   // store logs to an array for reference
  log.history.push(arguments);
  if(this.console){
    console.log( Array.prototype.slice.call(arguments) );
  }
};



// catch all document.write() calls
(function(doc){
  var write = doc.write;
  doc.write = function(q){ 
    log('document.write(): ',arguments); 
    if (/docwriteregexwhitelist/.test(q)) write.apply(doc,arguments);  
  };
})(document);


