
/**
 * execute requests
 */
$(document).ready(function() {
	
	$('#ajaxLoadIndicator')
    .hide()  // hide it initially
    .ajaxStart(function() {
        $(this).show();
    })
    .ajaxStop(function() {
        $(this).hide();
    });


	doBrowse();

});

// http://stackoverflow.com/questions/646628/javascript-startswith
if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.slice(0, str.length) == str;
  };
}

if (typeof String.prototype.endsWith != 'function') {
  String.prototype.endsWith = function (str){
    return this.slice(-str.length) == str;
  };
}


function doBrowse() {
	log(window.location);
	var id = decodeURIComponent($.query.get('id'));
	
	if(id=="") {
		if(window.location.pathname.startsWith("/textgrid:")) {
			id=window.location.pathname.substring(1);
		}
	}
	
	//window.log(id);
	if(id=='') {	
		$('#infoBox').html('<p><strong>No aggregation given, try e.g. <a href="browse.html?id=textgrid:dctj.0">textgrid:dctj.0</a></p>');
		return;
	}
	
	$('#mdlink').append('<ul><li><a href="'+tgcrudMetaDataLink(id)+'">View metadata</a></li></ul>')
	
	//var infoRequest = 'http://'+window.location.hostname+'/tgsearch-public/info/'+id+'?path=true';
	var infoRequest = tgsearchURL + 'info/'+id+'?path=true';	
	//var infoRequest = 'responses/rdfparse.xml';
	
	$.ajax({
	  url: infoRequest,
	  dataType: 'xml',
	  success: function(xml){
		  respElem = $(xml).find('TGS|response');
		  
		  var title = respElem.find('TGM|title').text(),
		      titleHtml = title;
		  var path = showPath(xml);
		  var format = respElem.find('TGM|format').text();
          var pid = respElem.find('TGM|pid').text(),
	      depiction = respElem.find('[nodeName="tg:depiction"]'),
              sandbox = $(respElem.find('TGS|result')[0]).attr('sandbox'),
              projectid = respElem.find('TGM|project').attr('id'),
	      fragment = decodeURIComponent($.query.get('fragment')),
	      ftitle   = decodeURIComponent($.query.get('ftitle'));

          if(sandbox) {
            setSandboxLayout();
          }
		  
		  if(depiction.size() > 0) {
			  handleImage(depiction);
		  }

		  handlePid(pid);		  

		  // if there is a fragment, the current object's title should
		  // be a link that points to the browse page for the whole
		  // referenced document.
		  if (fragment !== "") {
		    titleHtml = "<a href='browse.html?id="+id+"'>"+title+"</a>"; // need templating here
		  } else {
		    titleHtml = title;
		  }

		  if(path.length > 0) {
			$('#infoHead').append(path + ' &gt; ' + titleHtml);
		  } else {
			$('#infoHead').append(title);
		  }

		  // Do we know the fragment's title? Append it to the path
		  if (ftitle !== "") {
		    $('#infoHead').append(' &gt; ' + ftitle);
		  }
		  
          //var editionData = handeEdition(xml);
          //$('#infoBox').append('<ul>'+editionData+'</ul>');
          
            /* fill browse & meta box depending on content type */
			if(format.search('aggregation') != -1) {
				if($(xml).find('edition').length > 0) {
					var metaEntry = handleEditionMeta(xml);
					$('#infoBox').append('<ul>'+metaEntry+'</ul>');
				}
				handleAggregation(id);
		    } else if(format== 'text/tg.work+xml') {
				var metaEntry = handleWorkMeta(xml);
				$('#infoBox').append('<ul>'+metaEntry+'</ul>');
				handleWork(id);
			} else if (format == 'text/xml') {
                // this is not very nice, we need a general way for identifying dfg-viewer-mets
                if(respElem.find('TGM|identifier').attr('type')=='METSXMLID') {
                    handleDfgMets(id);	
                }else {
    				handleTEI(id, title);
                }
				handleMirador(id, projectid);
			} else if (format.search('application/x-shockwave-flash') != -1) {
				handleSwf(id);
      } else if (format.search('image/svg') != -1) {
        handleSvg(id);
			} else if (format.search('image' != -1)) {
				handleImageBrowse(id);
			}
			         
      }
    });

}

function handlePid(pid, text) {

    if(!text) text = 'PID';
          if(pid.length > 0) {
              // TODO: pid type = handle?
              var hdlLink = '<a href="http://hdl.handle.net/'+pid.substr(4)+'">(resolve)</a>';
              $('#infoBox').append(createMetaEntry(text, pid + ' ' + hdlLink));
          }
}

function handleSwf(id) {
	
	//var swfLoc = 'http://textgrid-ws3.sub.uni-goettingen.de/tgcrud/rest/'+id+'/data';
	var swfLoc = tgcrudURL + 'rest/'+id+'/data';
	
	$('<div id="flash"></div>').appendTo('#browsing-tree').flash({
            'src': swfLoc,
            'width':'640',
            'height':'480'
	});
}

function addDownload(options) {
	var list = $('#downloadlinks'),
	    link = $('<a></a>');
	if (list.length === 0) {
		list = $('<ul id="downloadlinks" data-label="Download as: " class="downloads"></ul>').appendTo('#mdlink');
	}
	link.attr('href', options.href);
	link.text(options.text);
	if (options.classes) {
		link.attr('class', options.classes);
	}
	if (options.title) {
		link.attr('title', options.title);
	}
	if (options.id) {
		link.attr('id', options.id);
	}

	if (options.replace && list.find(options.replace).length > 0) {
		list.find(options.replace).replaceWith(link);
	} else if (options.amend && list.find(options.amend).length > 0) {
		list.find(options.amend).parent().append(" ").append(link);
	} else {
		$('<li></li>').appendTo(list).append(link);
	}
	
}

function handleAggregation(id) {

	addDownload({
		text: 'TEI Corpus',
		href: aggregatorURL + 'teicorpus/' + id,
		title: 'teiCorpus elements will be nested according to the hierarchical structure',
		classes: 'teicorpus',
		id: 'download-teicorpus-hier'});
	addDownload({
		text: '(flat)',
		href: aggregatorURL + 'teicorpus/' + id + '?flat=true',
		title: 'Only one teiCorpus element containing all TEI documents, the hierarchy is lost',
		classes: 'teicorpus flat',
		amend: '#download-teicorpus-hier'});
	addDownload({
		text: 'E-Book (EPUB)',
		href: aggregatorURL + 'epub/' + id,
		title: 'E-Book formatting is experimental!',
		classes: 'epub beta',
		id: 'download-epub-corpus'});
	addDownload({
		text: 'ZIP Archive',
		href: aggregatorURL + 'zip/' + id,
		title: 'ZIP export is experimental!',
		classes: 'zip beta',
		id: 'download-zip'});
	addDownload({
	        text: 'ZIP (configurable) …',
		href: 'zip.html?id='+id,
		title: 'ZIP with options such as plain-text transformation etc.',
		classes: 'zip alpha',
		id: 'download-zip-cfg'});
	
	//var aggNavRequest = "http://localhost/repo/webapp/responses/73xd.0.aggnav.xml";
	var aggNavRequest = 
//		'http://'+window.location.hostname+'/tgsearch-public/navigation/agg/'+id;
		tgsearchURL + 'navigation/agg/'+id;
	
	$.ajax({
	  url: aggNavRequest,
	  dataType: 'xml',
	  success: function(xml){
		  respElem = $(xml).find('[nodeName="tgs:response"]');
		  
		  var resultCount = $(xml).find('TGS|result').size();
		  
		  if(resultCount==0) {
			  $('#searchresults').html('<p>No browseable content found.</p>');
		  } else if( resultCount==1 ) {
			  // if only one item, look if xml, if yes try embedding xslt view
			  var res = $(xml).find('TGS|result').last();
			  window.log(res);
			  if($(res).find('TGM|format').text() == 'text/xml') {
				  var xmlUri = $(res).find('TGM|textgridUri').text();
				  var title = $(res).find('TGM|title').text();

				  handleTEI(xmlUri, title);

				  var pid = $(res).find('TGM|pid').text();
                  handlePid(pid, 'PID of TEI file');
			  } else {
				showHits(xml);
			  }
		  } else {
			  showHits(xml);
		  }
		  
	  }
	});
}

function handleWork(id) {

	var aggNavRequest = tgsearchURL + 'search/?q=isEditionOf:"'+id+'"';

	$.ajax({
	  url: aggNavRequest,
	  dataType: 'xml',
	  success: function(xml){
		  respElem = $(xml).find('[nodeName="tgs:response"]');
		  
		  //xmlc = prefixClean(xml);
		  
		  //$.xmlns.TGM = "http://textgrid.info/namespaces/metadata/core/2010";
		  //window.log($(xml).find('*|title').text());
		  var resultCount = showHits(xml);
		  if(resultCount==0) {
			  $('#searchresults').html('<p>No browseable content found.</p>');
		  }
	  }
	});
}

function handleTEI(id, title) {

	// var request = 'http://'+window.location.hostname+'/exist-public/rest/xslt/transform.xql?id='+id.substr(9);
  	// var request = existXsltURL + 'transform.xql?id='+id.substr(9);
	var fragment = decodeURIComponent($.query.get('fragment'));
	var request = aggregatorURL + "html/" + id + "?embedded=true&mediatype=text/xml";
	var linkURL = 'browse.html?id=@URI@&fragment=@ID@';
	request = request + '&linkPattern=' + encodeURIComponent(linkURL);
	if (fragment !== "") {
	  request = request + "&id=" + fragment;
	}

	window.log('try loading xslt');
	window.log(request);
	
	$('<ul/>').html($('<li/>').html(sendToVoyantLink(id))).appendTo('#mdlink');

	var basketState = '<input type="checkbox" id="basketState"/> in basket';
	$('<ul/>').html($('<li/>').html(basketState)).appendTo('#mdlink');
	var basketInput = $.find('#basketState')[0];
	basketInput.checked = basketContains(id);
	basketInput.removeAttribute('id');
	basketInput.onclick = function() {
		basketToggle(id, title);
		this.checked = basketContains(id);
	};

	addDownload({
		href: tgcrudDataLink(id),
		text: 'TEI',
		title: 'verbatim TEI document as stored in the repository',
		classes: 'tei'});
	addDownload({
		href: aggregatorURL + "epub/" + id,
		text: 'E-Book (EPUB)',
		title: 'E-Book formatting is experimental!',
		classes: 'epub beta',
		replace: '#download-epub-corpus'});
//	addDownload({
//		href: aggregatorURL + "pdf/" + id,
//		text: 'PDF',
//		title: 'PDF formatted via LaTeX. Extremely experimental.',
//		classes: 'pdf alpha'});
	var htmlLink = aggregatorURL + 'html/' + id;
	if (fragment !== "") { htmlLink = htmlLink + '?id=' + fragment; }

	addDownload({
	  href: htmlLink,
	  text: 'HTML',
	  title: 'Raw HTML view',
	  classes: 'html'});

	var browsingTree = $('#browsing-tree');
	browsingTree.append('<p class="pending">Loading …</p>');


	console.log("Now requesting HTML");
	$.ajax({
	  url: request,
	  dataType: 'html',
	  success: function(xml) { 
			console.log("Successfully requested HTML");
			browsingTree
			    .addClass('teiXsltView')
			    .html(xml);
	      
			// kwic show
			if($.query.get('match')) {
				var hash = window.location.hash.substring(1);
				document.getElementById(hash).scrollIntoView(true);
				$('div').highlight($.query.get('match'));
			}
	  },
	  error: function(xhr, textStatus, errorThrown) {
	    console.log("Failed to request HTML", textStatus, errorThrown);
	    var msg = $('<div class="alert error alert-error"></div>');
	    msg.append("<h4>" + errorThrown + "</h4>");
	    msg.append("<p>An error occurred rendering the embedded HTML version of this document. <a href='" + aggregatorURL + "html/" + id + "?pi=true' target='_top'>Click here to see the transformation result in a new window.</a></p>");
	    $("#browsing-tree").append(msg);
	  },

	});
	
}

function handleImageBrowse(id) {
	var browsingTree = $('#browsing-tree');

	browsingTree.append($('<img src="'+digilibURL+'/'+id+'?dw=400">'));
	
	/*var opts = {
		interactionMode : 'embedded',
		scalerBaseUrl : digilibURL,
		showRegionNumbers : false,
		autoRegionLinks : true
	};
	
	browsingTree.digilib(opts);*/
	
	// 
	var extLink = $('<a href="/digilib/jquery/digilib.html?fn='+id+'">View with DigiLib</a>')
	$('<ul/>').html($('<li/>').html(extLink)).appendTo('#mdlink');
	
}

function handleSvg(id) {
    var browsingTree = $('#browsing-tree');
    browsingTree.append($('<img src="'+tgcrudURL+'/rest/'+id+'/data">'));
}

function handleDfgMets(id) {
    var viewerUrl = dfgViewerURL + '?set[zoom]=min&set[mets]=' + encodeURIComponent(tgcrudDataLink(id));
    var link = $('<a>').attr('href', viewerUrl).html('View with DFG-Viewer');
	$('<ul/>').html($('<li/>').html(link)).appendTo('#mdlink').appendTo('#mdlink');
}

function handleMirador(id, projectid) {
	console.log("id: " + id + " - proj: " + projectid);
	$.getJSON(iiifProjectsURL, function(data) {
		if($.inArray(projectid, data.projects) >= 0) {
			var link = $('<a>').attr('href', miradorURL+id).html('View with Mirador');
			$('<ul/>').html($('<li/>').html(link)).appendTo('#mdlink');
		}
	});
}

// this is used for the author image in the "digitale bibliothek", for image browse look at handleImageBrowse()
function handleImage(depiction) {
	
	  // var imgId = $(depiction).attr('resource');
	  
	  // remove ns2,3,x prefixes from attribut
      // var snode = $('<div>').append($(depiction).clone()).remove().html();
    
   	  var imgId = attrByNamespace(depiction[0], 'resource', $.xmlns.RDF);
	  
	  if(imgId.indexOf('.')< 0) {
		  imgId += '.0';
	  }
	  
	  window.log('imgid: ' + imgId);
	  var imgLink = tgcrudDataLink(imgId);
	  
	  var imgBox = $('<div></div>').append('<img id="depiction" src="'+imgLink+'" alt="[img in meta, id is: '+imgId+']" />');
	  //imgBox.css('float', 'right');
	  imgBox.addClass('imgBox');

	  //var infoRequest = 'http://'+window.location.hostname+'/tgsearch-public/info/'+imgId+'?path=false';
	  var infoRequest = tgsearchURL + 'info/'+imgId+'?path=false';
	  
	$.ajax({
	  url: infoRequest,
	  dataType: 'xml',
	  success: function(xml) {
		  //var res;
		  //respElem = $(xml).find('[nodeName="tgs:response"]');
		  
		  //var res = extractMeta(xml, 'title', '');
		  //res += extractMeta(xml, 'notes', 'Additional Information');
		  var res = $(xml).find('title').text();
		  
		  $(imgBox).append('<ul>'+res+'</ul>');
		  $('#infoBox').prepend(imgBox);
		  
	  },
	});
}

function handleWorkMeta(xml) {
	var res = '';
	var agent = $(xml).find('agent');
	var title = $(xml).find('title');
	
	if($(agent).attr('role') == 'author') {
		res += createMetaEntry($(agent).text(), $(title).text());
	}

	var doc = $(xml).find('dateOfCreation');
	
	if($(doc).attr('notBefore') && $(doc).attr('notAfter')) {
		var dstring = $(doc).attr('notBefore') + '-' + $(doc).attr('notAfter');
		res += createMetaEntry('Approximate date of creation', dstring);
	} else {
		res += createMetaEntry('Approximate date of creation', $(doc).text());
	}
	
	res += extractMeta(xml, 'notes', 'Additional Information', $(doc).text());
//	res += extractMeta(xml, 'license', 'License');
	return res;
}

function handleEditionMeta(xml) {
	
	var res = '';
	
        res += extractMeta(xml, 'editionTitle', 'Edition');
        res += extractMeta(xml, 'dateOfPublication', 'Date of publication');
	res += extractMeta(xml, 'notes', 'Additional Information');
        res += addAuthor(xml);
//	res += extractMeta(xml, 'license', 'License');
	
	window.log(xml);
	
	var workid = $(xml).find('isEditionOf').text();
	window.log('workid: ' + workid);
	//var infoRequest = 'http://'+window.location.hostname+'/tgsearch-public/info/'+workid;
	var infoRequest = tgsearchURL + 'info/'+workid;

	$.ajax({
		url: infoRequest,
		dataType: 'xml',
		success: function(xml){
			respElem = $(xml).find('[nodeName="tgs:response"]');
			var workMeta = handleWorkMeta(xml);
			$('#infoBox').prepend('<ul>'+workMeta+'</ul>');
			
		}
	});
	
	
	
	//var date = $(xml).find('issued').text();
	//log(date);
	//res += createMetaEntry('Published in TextGrid', formatDate(date));
	
	
/*	ed = $(xml).find('edition');
	if(ed.length == 0) {
		window.log('no edition');
		return res;
	}
	
	//var agent = $(xml).find('agent');
	//res += createMetaEntry('Agent (' + $(agent).attr('role') + ')', $(agent).text());

	res += extractMeta(xml, 'editionTitle', 'Edition');
	res += extractMeta(xml, 'placeOfPublication', 'Place of publication');
	res += extractMeta(xml, 'dateOfPublication', 'Date of publication');
	res += extractMeta(xml, 'license', 'License');
	res += extractMeta(xml, 'pid', 'Persistent Identifier');
*/	
	return res;
}

function addAuthor(xml) {
        var res='';
        $(xml).find('author').each(function() {
                var id = $(this).attr('id');
                var name = $(this).text();

                if(id && id.startsWith('pnd:')) {
                        var pnd = id.substr(4);
                        name += ' (PND: <a href="' + pndResolveURL + pnd + '" target="blank">' + pnd +'</a>)'
                }
                console.log('id:' + id + ' name: ' + name);
                res +=  createMetaEntry('Author', name);
        });
        return res;
}

function extractMeta(xml, toExtract, title) {
	var res = '';
	$(xml).find(toExtract).each(function() {
		if($(this).text()) {
			res += createMetaEntry(title, $(this).text());
		}
	});
	return res;
}

function createMetaEntry(title, text) {
			var res = '<li><dl class="meta">';
			res += '<dt>'+title+':</dt><dd>' + text + '</dd>';
			res += '</dl></li>';
			return res
}
