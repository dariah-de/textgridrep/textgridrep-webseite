
$(document).ready(function() {
    
    $('#ajaxLoadIndicator')
	.hide()  // hide it initially                                                                                        
	.ajaxStart(function() {
            $(this).show();
	})
	.ajaxStop(function() {
            $(this).hide();
	});
    
    // Get all URIs from basket.
    var uris = '';
    $(readBasket()).each(function () {
	uris += '&uri='+this.id;
    });

    // Get vars from form in basket.html.
    
    var responseMimetype = decodeURIComponent($.query.get('responseMimetype'));
    var preAdaptorUri = decodeURIComponent($.query.get('preAdaptorUri'));
    var postAdaptorUri = decodeURIComponent($.query.get('postAdaptorUri'));
    var splitTag = decodeURIComponent($.query.get('splitTag'));
    var algorithm = decodeURIComponent($.query.get('algorithm'));
    var comparator = decodeURIComponent($.query.get('comparator'));
    var distance = decodeURIComponent($.query.get('distance'));
    
    if (responseMimetype == "true") {
	XresponseMimetype = "";
    } else {
	XresponseMimetype = "responseMimetype=" + encodeURIComponent(responseMimetype);
    }
    if (preAdaptorUri == "true") {
	XpreAdaptorUri = "";
    } else {
	XpreAdaptorUri = "&preAdaptorUri=" + encodeURIComponent(preAdaptorUri);
    }
    if (postAdaptorUri == "true") {
	XpostAdaptorUri = "";
    } else {
	XpostAdaptorUri = "&postAdaptorUri=" + encodeURIComponent(postAdaptorUri);
    }
    if (splitTag== "true") {
	XsplitTag = "";
    } else {
	XsplitTag = "&splitTag=" + encodeURIComponent(splitTag);
    }
    if (algorithm == "true") { 
	Xalgorithm = "";
    } else {
	Xalgorithm = "&algorithm=" + encodeURIComponent(algorithm);
    }
    if (comparator == "true") { 
	Xcomparator = "";
    } else {
	Xcomparator = "&comparator=" + encodeURIComponent(comparator);
    }
    if (distance == "true") {
	Xdistance = "";
    } else {
	Xdistance = "&distance=" + encodeURIComponent(distance);
    }
    
    // console.log("uris: " + uris);
    // console.log("responseMimetype: " + responseMimetype);
    // console.log("preAdaptorUri: " + preAdaptorUri);
    // console.log("postAdaptorUri: " + postAdaptorUri);
    // console.log("splitTag: " + splitTag);
    // console.log("algorithm: " + algorithm);
    // console.log("comparator: " +comparator);
    // console.log("distance: " + distance);
    
    var reqString = collatexWrapURL + "collate?" + XresponseMimetype + XpreAdaptorUri + XpostAdaptorUri + XsplitTag + Xalgorithm + Xcomparator + Xdistance + "&joined=true" + uris;

    // console.log(reqString);
    
    // Dependent on response mimetype, do a redirect or a HTTP GET.
    if (responseMimetype == "text/html") {
	$.ajax({
	    type: "GET",
	    contentType: "application/json;charset=UTF-8",
	    accept: responseMimetype,
	    url: reqString,
	    success: function(response) {
		var nyi = "Returning mimetype <b>" + responseMimetype + "</b> is not yet implemented on client side!";
		var table = $(response).find("table");
		$('#collatex-table').html($(response).append(table));
	    },
	    error: function(response) {
		// var error = "<b>AN UNEXPECTED ERROR OCCURED!</b> <br/><br/> We apologize for the inconvenience.";
		// $('#pleaseBePatient').html(error);
		// var errorhead = $(response).find("head");
		// var errorbody = $(response).find("body");
		// $('#collatex-head').html($(response).append(errorhead));
		// $('#collatex-body').html($(response).append(errorbody));
		$('html').html(response.responseText);
	    }
	} );
    }
    else {
	document.location.href = reqString;
    }
    
    //	else if (responseMimetype == "application/json") {
    //		console.log("MIME: " + responseMimetype);
    //		console.log("RESULT: " + $(response));
    //		$('#pleaseBePatient').html(nyi);
    //	    }
    //	    else if (responseMimetype == "application/tei+xml") {
    //		console.log("MIME: " + responseMimetype);
    //		console.log("RESULT: " + $(response));
    //		$('#pleaseBePatient').html(nyi);
    //	    }
    //	    else if (responseMimetype == "application/graphml+xml") {
    //		console.log("MIME: " + responseMimetype);
    //		console.log("RESULT: " + $(response));
    //		$('#pleaseBePatient').html(nyi);
    //	    }
    //	    else if (responseMimetype == "text/plain") {
    //		console.log("MIME: " + responseMimetype);
    //		console.log("RESULT: " + $(response));
    //		$('#pleaseBePatient').html(nyi);
    //	    }
    //	    else if (responseMimetype == "image/svg+xml") {
    //		console.log("MIME: " + responseMimetype);
    //		console.log("RESULT: " + $(response));
    //		$('#pleaseBePatient').html(nyi);
    //	    }
    
} );
