/**
 *  The different Voyant tools.
 */
SEND_TO_PAGE = { label: 'Explore with DIGIVOY', url: "sendto.html" };
VoyantTools = {
	MAIN: { label: 'Voyant', url: voyantURL },
	BUBBLELINES: { label: 'Bubblelines', url: voyantURL + "tool/Bubblelines/" },
	BUBBLES: { label: 'Bubbles', url: voyantURL + "tool/Bubbles/" },
//	CENTROID: { label: 'Centroid', url: voyantURL + "tool/Centroid/" },
	CIRRUS: { label: 'Cirrus', url: voyantURL + "tool/Cirrus/" },
	CORPUS_GRID: { label: 'Corpus Grid', url: voyantURL + "tool/CorpusGrid/" },
	CORPUS_SUMMARY: { label: 'Corpus Summary', url: voyantURL + "tool/CorpusSummary/" },
	CORPUS_TYPE_FREQUENCIES_GRID: { label: 'Corpus Type Frequencies Grid', url: voyantURL + "tool/CorpusTypeFrequenciesGrid/" },
//	DOCUMENT_INPUT_ADD: { label: 'Document Input Add', url: "/voyant/tool/DocumentInputAdd/" },
	DOCUMENT_TYPE_COLLOCATE_FREQUENCIES_GRID: { label: 'Document Type Collocate Frequencies Grid', url: voyantURL + "tool/DocumentTypeCollocateFrequenciesGrid/" },
	DOCUMENT_TYPE_FREQUENCIES_GRID: { label: 'Document Type Frequencies Grid', url: voyantURL + "tool/DocumentTypeFrequenciesGrid/" },
	DOCUMENT_TYPE_KWICS_GRID: { label: 'Document Type Kwics Grid', url: voyantURL + "tool/DocumentTypeKwicsGrid/" },
//	ENTITIES_BROWSER: { label: 'Entities Browser', url: voyantURL + "tool/EntitiesBrowser/" },
//	EQUALIZER: { label: 'Equalizer', url: voyantURL + "tool/Equalizer/" },
//	FEATURE_CLUSTERS: { label: 'Feature Clusters', url: voyantURL + "tool/FeatureClusters/" },
//	FLOWERBED: { label: 'Flowerbed', url: voyantURL + "tool/Flowerbed/" },
	KNOTS: { label: 'Knots', url: voyantURL + "tool/Knots/" },
//	KWICS_TAGGER: { label: 'Kwics Tagger', url: voyantURL + "tool/KwicsTagger/" },
	LAVA: { label: 'Lava', url: voyantURL + "tool/Lava/" },
	LINKS: { label: 'Links', url: voyantURL + "tool/Links/" },
//	MICRO_SEARCH: { label: 'Micro Search', url: voyantURL + "tool/MicroSearch/" },
	NET_VIZ_APPLET: { label: 'NetViz Applet', url: voyantURL + "tool/NetVizApplet/" },
//	PAPER_DRILL: { label: 'Paper Drill', url: voyantURL + "tool/PaperDrill/" },
	READER: { label: 'Reader', url: voyantURL + "tool/Reader/" },
//	REZO_VIZ: { label: 'RezoViz', url: voyantURL + "tool/RezoViz/" },
	SCATTER_PLOT: { label: 'Scatter Plot', url: voyantURL + "tool/ScatterPlot/" },
//	SUNBURST: { label: 'Sunburst', url: voyantURL + "tool/Sunburst/" },
//	TICKER: { label: 'Ticker', url: voyantURL + "tool/Ticker/" },
//	TOKENS_VIZ: { label: 'TokensViz', url: voyantURL + "tool/TokensViz/" },
	TERMOMETER: { label: 'Termometer', url: voyantURL + "tool/Termometer/" },
	TERMSRADIO: { label: 'TermsRadio', url: voyantURL + "tool/TermsRadio/" },
//	TOOL_BROWSER: { label: 'ToolBrowser', url: voyantURL + "tool/ToolBrowser/" },
//	TOOL_BROWSER_LARGE: { label: 'ToolBrowserLarge', url: voyantURL + "tool/ToolBrowserLarge/" },
	TYPE_FREQUENCIES_CHART: { label: 'Type Frequencies Chart', url: voyantURL + "tool/TypeFrequenciesChart/" },
	VISUAL_COLLOCATOR: { label: 'Visual Collocator', url: voyantURL + "tool/VisualCollocator/" },
//	VOYEUR_FOOTER: { label: 'VoyeurFooter', url: voyantURL + "tool/VoyeurFooter/" },
//	VOYEUR_HEADER: { label: 'VoyeurHeader', url: voyantURL + "tool/VoyeurHeader/" },
//	VOYEUR_TAGLINE: { label: 'VoyeurTagline', url: voyantURL + "tool/VoyeurTagline/" },
//	WORD_CLOUD: { label: 'Word Cloud', url: voyantURL + "tool/WordCloud/" },
	WORD_COUNT_FOUNTAIN: { label: 'Word Count Fountain', url: voyantURL + "tool/WordCountFountain/" }
};

function initToolSelect(selectElement)
{	
	var options = [];
	$.each(VoyantTools, function(index, data) {
		options.push('<option value="', data.url, '">', data.label, '</option>');
	});
	selectElement.html(options.join(''));
}

/**
 * Send a single document to Voyant and open Voyant afterwards.
 * 
 * @param toolUrl the URL of the Voyant Tool to open after sending the document
 * @param documentUrl the full public URL of the document to open
 */
function sendToVoyant(toolUrl, documentUrl)
{
	$.ajax({
		url: voyantURL + "trombone/",
		data: {
			input: documentUrl,
			// DIGIVOY-34: only text, no stuff like notes or speakers
			xmlContentXpath: "/*[local-name()='TEI']/*[local-name()='text']/*[local-name()='body']",
			// DIGIVOY-33: extract title information from TEI
			xmlTitleXpath: "/*[local-name()='TEI']/*[local-name()='teiHeader']/*[local-name()='fileDesc']/*[local-name()='titleStmt']/*[local-name()='title']",
			// DIGIVOY-33: extract author information from TEI
			xmlAuthorXpath: "/*[local-name()='TEI']/*[local-name()='teiHeader']/*[local-name()='fileDesc']/*[local-name()='sourceDesc']/*[local-name()='biblFull']/*[local-name()='titleStmt']/*[local-name()='author']",
			// DIGIVOY-33: extract publication time
			xmlTimeXpath: "/*[local-name()='TEI']/*[local-name()='teiHeader']/*[local-name()='fileDesc']/*[local-name()='sourceDesc']/*[local-name()='biblFull']/*[local-name()='publicationStmt']/*[local-name()='date']/attribute::*[local-name()='when']",
			timePattern: "yyyy",
			timeLocaleLanguage: "de",
			timeLocaleCountry: "DE",
		},
		dataType: 'json',
		success: function(json) {
			window.open(toolUrl+'?corpus='+json.corpusId+'&stopList=stop.de.german.txt', '_self', false);
		},
        error: function(xhr, txt){
            $('#searchresults').append('<li class="error">' + txt + '</li>');
        }
	});
}

/**
 * Create a new link to the "send to" page to customize the sending process before actually
 * sening the document to Voyant.
 * 
 * @param aDocumentId the id of the document to open.
 * @returns a HTML a element with an onclick handler that opens the docuemnt in Voyant.
 */
function sendToVoyantLink(aDocumentId)
{
	var params = {documentId: aDocumentId};
	return $('<a/>', {
	    text: "Explore with DIGIVOY",
	    title: "Explore with DIGIVOY",
	    href: "sendto.html?"+$.param(params)
	});
}

/**
 * Create a new link to the "send to" page to customize the sending process before actually
 * sening the document to Voyant.
 * 
 * @param aDocumentId the id of the document to open.
 * @returns a HTML a element with an onclick handler that opens the docuemnt in Voyant.
 */
function sendToVoyantLinkHtml(aDocumentId)
{
	var params = {documentId: aDocumentId};
	return "<a href='sendto.html?"+$.param(params)+"'>Explore with DIGIVOY";
}

function filteredTGCrudDataLink(id, query) {
	var link = "http://localhost:8080/xsltws/filter?id="+id+"&i=teiHeader";
	if (query) {
		link += "&"+query;
	}
	return link;
}

