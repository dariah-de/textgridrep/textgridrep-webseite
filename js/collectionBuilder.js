KEY_CB_DOCS = 'textgrid.collectionBuilder.documentIds';

function basketContains(aDocumentId)
{
	var documentIds = readBasket();
	
	if (!documentIds) {
		return false;
	}
	else {
		for (var i = 0; i < documentIds.length; i++) {
			if (documentIds[i].id == aDocumentId) {
				return true;
			}
		}
		return false;
	}
}

function basketRemove(aDocumentId)
{
	var documentIds = readBasket();
	
	documentIds = $.grep(documentIds, function(doc) {
	    return doc.id != aDocumentId;
	});
		
	writeBasket(documentIds);
	
	updateBasketTray();
}

function basketAdd(aDocumentId, aTitle)
{
	var documentIds = readBasket();
	
	if (!documentIds) {
		documentIds = [];
	}
	
	documentIds.push({id: aDocumentId, title: aTitle});
	
	writeBasket(documentIds);
	
	updateBasketTray();
}

function basketToggle(aDocumentId, aTitle)
{
	if (basketContains(aDocumentId)) {
		basketRemove(aDocumentId);
	}
	else {
		basketAdd(aDocumentId, aTitle);
	}
}

function clearBasket()
{
	localStorage[KEY_CB_DOCS] = [];
	updateBasketTray();
}

function readBasket()
{
	var raw = localStorage[KEY_CB_DOCS];
	if (raw) {
		try {
			return JSON.parse(raw);
		}
		catch (e) {
			return [];
		}
	}
	else {
		return [];
	}
}

function writeBasket(aBasket)
{
	localStorage[KEY_CB_DOCS] = JSON.stringify(aBasket);
}

function updateBasketTray()
{
	var documentIds = readBasket();
	if (documentIds.length > 0) {
		$('#basketTray').html('&nbsp;('+readBasket().length + ')');
	}
	else {
		$('#basketTray').html('');
	}	
}

$(document).ready(function () {
	updateBasketTray();
});