

$(document).ready(function() {

	if($.query.get("formSubmit")) {
		
		/**
		 * handle mdquery
		 */
		var query = "";
		var mdvars = $.query.get("metadata");
		
		$.each(mdvars, function(index, value) {
			if(typeof(value.word[value.word.length-1]) != "boolean" ) {
				query += "(";
			}
			$.each(value.word, function(windex, word) {
				console.log(word);
				console.log(typeof(word));
				//if(! $.isEmptyObject(word)) {
				// empty fields are set as true for some reason
				if(word != '' && word != true) {
					query += value.field + ":\"" + word + "\" OR ";
				}
			});
			
			// cut the last "or"
			query = query.substring(0, query.length-4);
			if(typeof(value.word[value.word.length-1]) != "boolean" ) {
				query += ")";
			}
			query += " AND ";
		});
		
		// cut the last "and"
		query = query.substring(0, query.length-5);
		
		/**
		 * handle content type query
		 * todo: what if nothing selected?
		 */
		if($.query.get("ctypeSel") == "some") {
			
			var ctypeArr = $.query.get("contenttype");
			
			if(query != "") query += " AND ";
			if(ctypeArr.length > 1) {
				query += "(";
			}
			$.each(ctypeArr, function() {
				 query += "format:\""+this+"\" OR "
			});
			
			// cut the last "or"
			query = query.substring(0, query.length-4);

			if(ctypeArr.length > 1) {
				query += ")";
			}
		}
		
		/**
		 * language
		 */
/*		if($.query.get("langSel") == "some") {
			
			var langArr = $.query.get("language");
			
			if(query != "") query += " and ";
			$.each(ctypeArr, function() {
				 query += "language:\""+this+"\" or "
			});
			
			// cut the last "or"
			query = query.substring(0, query.length-4);
		}*/
		
		/**
		 * fulltext
		 */
		var fulltext = $.query.get("fulltext");
		if($.query.get("fulltextSel")  == "active" && fulltext != "") {
			if(! $.isEmptyObject(fulltext)) {
				if(query != "") {
					query += " AND ";
				}
				
		        if( $.query.get("wordDistanceSel")=="true") {
			        var wd = $.query.get("wordDistance")
                    query += '"'+fulltext+'"~'+wd;
		        } else {
                    query += fulltext;

                }
			}
		}
		

		
		console.log(query);
		
		location.href="results.html?query="+encodeURIComponent(query)+"&target=both";
	}
});


