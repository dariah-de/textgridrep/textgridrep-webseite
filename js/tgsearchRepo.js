$(document).ready(function() {
	
	$('#ajaxLoadIndicator')
    .hide()  // hide it initially
    .ajaxStart(function() {
        $(this).show();
    })
    .ajaxStop(function() {
        $(this).hide();
    });

	listRepo();

});

function listRepo() {

    var request = tgsearchURL+'/navigation/toplevel/min/'
    if(sandbox) request += '?sandbox=true';	
		
	$.ajax({
		url: request,
		dataType: 'xml',
		success: function(xml){				  
			$(xml).find('item').each(function(){
			    var id = $(this).attr('textgridUri');
			    var title = $(this).find('title').text();
			    var htmlClass = "";
			    if($(this).find('isNearlyPublished').length > 0) {
				    htmlClass = 'nearlyPublished';
			    } else if (sandbox) {
				    htmlClass = 'published';
                }
			    htmlClass = ' class="'+htmlClass+'"';
			    var link = 'browse.html?id='+id;
			    var htmlItem = $('#searchresults').append('<li'+htmlClass+'><h4><a href="'+link+'">'+title+'</a></h4></li>');
			});
		},
        error: function(xhr, txt){
            $('#searchresults').append('<li class="error">' + txt + '</li>');
        }
	});
	
}
