
// sessionId is kept here if et
var sessionid = '';

/**
 * execute requests
 */
$(document).ready(function() {
	
	/*$("#kwicSlider").slider( {
		min: 10,
		max: 40,
	});*/
	
	$('#ajaxLoadIndicator')
    .hide()  // hide it initially
    .ajaxStart(function() {
        $(this).show();
    })
    .ajaxStop(function() {
        $(this).hide();
    });

	
	if($.query.get('orderBy')) {
		window.log($.query.get('orderBy'));
		var arr = $.query.get('orderBy').split(':');
		$('#search-resultlist-order').val(arr[1]);
		$('#search-resultlist-ascdesc').val(arr[0]);
	}
	
	$('a.save-all-results').attr('href', 'zip.html?query='+$.query.get('query')+'&target='+$.query.get('target'));

	doQuery();

	if($('#search-resultlist-order').val()=='relevance') {
		$('#search-resultlist-ascdesc').hide();
	} else {
		$('#search-resultlist-ascdesc').show();
	}
	
	$('#search-resultlist-ascdesc').change(function() {
		doQuery();
	});
	
	$('#search-resultlist-order').change(function() {
		if($('#search-resultlist-order').val()=='relevance') {
			$('#search-resultlist-ascdesc').hide();
		} else {
			$('#search-resultlist-ascdesc').show();
		}
		doQuery();
	});

});

function doQuery() {
	
    var query = decodeURIComponent($.query.get('query'));
    var target = $.query.get('target');


    $('#queryfield').val(query);
    $('#radio_' + target).attr("checked","checked");
	
	window.log("q:" +query);
	
	if(query=='') {
		$('#searchresults').append(	'<p>no query, search with ' 
								  + '<a href="index.html">simple search</a> '
								  + 'or <a href="advanced.html">advanced search</a></p>');
		return;
	}
	
	window.log('page:' + $.query.get('page'));
	
	var page =  $.query.get('page') ? $.query.get('page') : 0;
	var path = $.query.get('path') ? $.query.get('path') : true;
	var limit = 10;
	var start = page * limit; 
	var wordDistance = $.query.get('wordDistance') ? $.query.get('wordDistance') : -1;
	
	/* orderby */
/*	if($.query.get('orderBy')) {
		var orderBy = $.query.get('orderBy');
	} else */
	if( $('#search-resultlist-order option:selected').text()=='relevance') {
		var orderBy = 'relevance';
	} else {
		var orderBy = $('#search-resultlist-ascdesc option:selected').val() + ':' + $('#search-resultlist-order option:selected').text();
	}
	//$.query.set('orderBy', orderBy);
	// the tgsearch session id, 
	var tgsid = $.query.get('tgsid') ? $.query.get('tgsid') : sessionid;
		
	var tgsearchRequest = 	
//				  'http://'+window.location.hostname+'/tgsearch-public/search?q='+query 
				tgsearchURL + 'search?q='+query
				+ '&target='+target
				+ '&limit=' +limit 
				+ '&start=' +start
				+ '&wordDistance=' + wordDistance 
				+ '&order=' + orderBy
				+ '&sessionid=' + tgsid
				+ '&path=' + path;
				
	if(sandbox) tgsearchRequest += "&sandbox=true";
	
	window.log(tgsearchRequest);
				
	//var tgsearchRequest = "http://localhost/repo/webapp/response.xml";
				
	$('#searchresults').attr('start', start + 1);
    
    //var pagerLink  = $.query.SET("page", "__id__").toString();
    
    $.ajax({
	  url: tgsearchRequest,
	  dataType: 'xml',
	  success: function(xml){
		
		// clean from old  
		$('#searchresults').html('');

		respElem = $(xml).find('TGS|response');
		var hits = $(respElem).attr('hits');
		sessionid = $(respElem).attr('session');
		
		var start = $(respElem).attr('start');

		if(hits < 1) {
		  showNoHits(query);
		}
		
		//window.log("hits:" +hits);
		//window.log("limit:" +limit);
		
		var end= Number(start)+Number(limit);
		$('#resultnumbers').html(start + '-' + end + ' of ' + hits);

		$('.pagertest').pagination(hits, {
			items_per_page: limit,
			prev_text: 'Previous Page',
			next_text: 'Next Page',
			renderer: 'tgRenderer',
			link_to: $.query.set('page', '__id__').set('tgsid', sessionid).set('orderBy', orderBy).toString(),
			current_page : page,
		    callback: function(){return true;}
		});
		
		window.log(xml);
		
		showHits(xml);
		
		/*$(xml).find('[nodeName="tgs:result"]').each(function(){
			
			//window.log(this);
			
			var title = $(this).find('[nodeName="title"]').text();
			var format = $(this).find('[nodeName="format"]').text();
			var uri = $(this).find('[nodeName="textgridUri"]').text();
			var date = $(this).find('issued').text();
			var extent = $(this).find('extent').text();
			var size = $.CalcSize(extent);
			
			var pathres = "";
			$(this).find('[nodeName="tgs:pathGroup"]').each(function() {
				window.log(this);
				$(this).find('[nodeName="tgs:path"]').each(function() {
					var path = "";
					$(this).find('[nodeName="tgs:entry"]').each(function() {
						var title = $(this).find('[nodeName="tgs:title"]').text();
						var uri = $(this).find('[nodeName="tgs:textgridUri"]').text();
						var link = "http://textgrid-ws3.sub.uni-goettingen.de/tgcrud/rest/"+uri+"/metadata";
					
						path += "<a href='"+link+"'>" + title + "</a> &gt; ";
					});	
					//path = path.substring(0, path.length-6);
					var link = 'http://textgrid-ws3.sub.uni-goettingen.de/tgcrud/rest/'+uri+'/metadata';
					path += '<a href="'+link+'">' + title + '</a>';
					pathres = '<dl class="meta"><dt>path:</dt><dd>'+path+'</dd></dl>';
				});
				
			});
			
			
			var kwic = '';
			$(this).find('[nodeName="tgs:kwic"]').each(function() {
				kwic += '<li>';
				kwic += $(this).find('[nodeName="tgs:left"]').text();
				kwic += '<strong class="red">' + $(this).find('[nodeName="tgs:match"]').text() + '</strong>';
				kwic += $(this).find('[nodeName="tgs:right"]').text();
				kwic += '</li>';
			});
			
			var kwicbox = '';
			if(kwic != '') {
				//kwicbox =  '<div class="expendable">KWIC';
				kwicbox = '<ol class="resultset">' +  kwic + '</ol></div>';
			} 
			
            var xslt = "";
			if(format=="text/xml") {
               var id = uri.substr(9);
               var xsltUrl = 'http://textgrid-ws3.sub.uni-goettingen.de/exist-public/rest/xslt/transform.xql';
               xslt = '<dt>view:</dt><dd><a href="'+xsltUrl+'?id='+id+'">html</a>'
                    + '&nbsp;<a href="'+xsltUrl+'?id='+id+'&sheet=fo2">fo</a></dd>';
            }
			
			$('#searchresults').append(   '<li><h4><a href="http://textgrid-ws3.sub.uni-goettingen.de/tgcrud/rest/'+uri+'/data">'+title+'</a></h4>'
										+ '<dl class="meta">'
										+ '<dt>format:</dt><dd>'+format+'</dd>'
										+ '<dt>published:</dt><dd>'+date+'</dd>'
										+ '<dt>size:</dt><dd>'+size+'</dd>'
										+ xslt
										+ '</dl>'
										+ pathres
										+ kwicbox
										+ '</li>');
		});*/
		  
		//$('.resultset .kwic').before('');	
	  }
	});

}


/**
 * @class Default renderer for rendering pagination links
 */
$.PaginationRenderers.tgRenderer = function(maxentries, opts) {
	this.maxentries = maxentries;
	this.opts = opts;
	this.pc = new $.PaginationCalculator(maxentries, opts);
}
$.extend($.PaginationRenderers.tgRenderer.prototype, {
	/**
	 * Helper function for generating a single link (or a span tag if it's the current page)
	 * @param {Number} page_id The page id for the new item
	 * @param {Number} current_page 
	 * @param {Object} appendopts Options for the new item: text and classes
	 * @returns {jQuery} jQuery object containing the link
	 */
	createLink:function(page_id, current_page, appendopts){
		var lnk, np = this.pc.numPages();
		page_id = page_id<0?0:(page_id<np?page_id:np-1); // Normalize page id to sane value
		appendopts = $.extend({text:page_id+1, classes:""}, appendopts||{});
		if(page_id == current_page){
			lnk = $("<span class='current'>" + appendopts.text + "</span>");
		}
		else
		{
			lnk = $("<a>" + appendopts.text + "</a>")
				.attr('href', this.opts.link_to.replace(/__id__/,page_id));
		}
		if(appendopts.classes){ lnk.addClass(appendopts.classes); }
		lnk.data('page_id', page_id);
		return lnk;
	},
	// Generate a range of numeric links 
	appendRange:function(container, current_page, start, end, opts) {
		var i;
		for(i=start; i<end; i++) {
			var item = $("<li></li>").appendTo(container);
			this.createLink(i, current_page, opts).appendTo(item);
		}
	},
	getLinks:function(current_page, eventHandler) {
		var begin, end,
			interval = this.pc.getInterval(current_page),
			np = this.pc.numPages(),
			fragment = $("<ul class='pager'></ul>");
		
		// Generate "Previous"-Link
		if(this.opts.prev_text && (current_page > 0 || this.opts.prev_show_always)){
			var item = $("<li></li>").appendTo(fragment);
			item.append(this.createLink(current_page-1, current_page, {text:this.opts.prev_text, classes:"prev"}));
		}
		// Generate starting points
		if (interval.start > 0 && this.opts.num_edge_entries > 0)
		{
			end = Math.min(this.opts.num_edge_entries, interval.start);
			this.appendRange(fragment, current_page, 0, end, {classes:'sp'});
			if(this.opts.num_edge_entries < interval.start && this.opts.ellipse_text)
			{
				jQuery("<li>"+this.opts.ellipse_text+"</li>").appendTo(fragment);
			}
		}
		// Generate interval links
		this.appendRange(fragment, current_page, interval.start, interval.end);
		// Generate ending points
		if (interval.end < np && this.opts.num_edge_entries > 0)
		{
			if(np-this.opts.num_edge_entries > interval.end && this.opts.ellipse_text)
			{
				jQuery("<li>"+this.opts.ellipse_text+"</li>").appendTo(fragment);
			}
			begin = Math.max(np-this.opts.num_edge_entries, interval.end);
			this.appendRange(fragment, current_page, begin, np, {classes:'ep'});
			
		}
		// Generate "Next"-Link
		if(this.opts.next_text && (current_page < np-1 || this.opts.next_show_always)){
			var item = $("<li></li>").appendTo(fragment);
			item.append(this.createLink(current_page+1, current_page, {text:this.opts.next_text, classes:"next"}));
		}
		$('a', fragment).click(eventHandler);
		return fragment;
	}
});

function showNoHits(q) {
	$('#search-resultlist').html('<div class="expandable-content"><p><strong>No results found searching for:</strong> "'+decodeURI(q)+'"</p></div>');
}



/**
 * jquery plugin for retrieving url params,
 * copied from  
 * http://stackoverflow.com/questions/901115/get-querystring-values-in-javascript
 * 
 * usage: 
 * 		var param = $.QueryString["param"]
 */
 /*
(function($) {
    $.QueryString = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i)
        {
            var p=a[i].split('=');
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'))
})(jQuery);
*/
