$.xmlns.TGM = "http://textgrid.info/namespaces/metadata/core/2010";
$.xmlns.TGS = "http://www.textgrid.info/namespaces/middleware/tgsearch";
$.xmlns.RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
function showHits(xml) {
		
		// declare textgrid namespaces
		//$.xmlns.TGM = "http://textgrid.info/namespaces/metadata/core/2010";
		//$.xmlns.TGS = "http://www.textgrid.info/namespaces/middleware/tgsearch";
		
		var resultNodes = $(xml).find('TGS|result');
			
		$(resultNodes).each(function(){
			
			//window.log(this);
			
			var title = $(this).find('TGM|title').text();
			var format = $(this).find('TGM|format').text();
			var uri = $(this).find('TGM|textgridUri').text();
			var date = $(this).find('TGM|issued').text();
			var pid = $(this).find('TGM|pid').text();
			var extent = $(this).find('TGM|extent').text();
			
			var path = showPath($(this));

			// set links
            
            var mainLink = '';
			if(format=='text/xml') {
               var id = uri.substr(9);
               //other = '<dt>view:</dt><dd><a href="'+xsltUrl+'?id='+id+'">html</a>'
                //    + '&nbsp;<a href="'+xsltUrl+'?id='+id+'&sheet=fo2">fo</a></dd>';
               //var mainLink = xsltUrl+'?id='+id;
               mainLink = 'browse.html?id='+uri;
            } else if(format.search('aggregation') != -1) {
				mainLink = 'browse.html?id='+uri;
			}else if(format=='text/tg.work+xml') {
				mainLink = 'browse.html?id='+uri;
			} else if(format.search('application/x-shockwave-flash') != -1) {
				mainLink = 'browse.html?id='+uri;
			} else if(format.search('image') != -1) {
				mainLink = 'browse.html?id='+uri;
				
			} else {
				//mainLink= 'http://textgrid-ws3.sub.uni-goettingen.de/tgcrud/rest/'+uri+'/data';
				mainLink = tgcrudURL + 'rest/'+uri+'/data';
			}

			// show kwic
			var kwic = '';
			$(this).find('TGS|kwic').each(function() {
				var match = $(this).find('TGS|match').text()
				if ($(this).attr('refId')) {
				  var refId = $(this).attr('refId'),
				      refTitle = $(this).attr('refTitle'),
				      refTitleClean = refTitle.replace(/<.*?>/g,''),
				      localLink = mainLink + 
					'&fragment=' + refId + 
					'&ftitle=' + refTitleClean + 
					'&match=' + match,
				      fragTitle = '<strong class="fragmentTitle">('+refTitle+')</strong>';
				} else {
				  var localLink = mainLink + '&match=' + match,
				      fragTitle = '';
				}
				kwic += '<li class="kwicresult">';
				kwic += $(this).find('TGS|left').text();
				kwic += '<strong class="red">' + match + '</strong>';
				kwic += $(this).find('TGS|right').text();
				//var nodeId = $(this).find('TGS|xpath').text();
				//window.log(nodeId);
				//kwic += ' <a href="'+mainLink+'&match='+match+'#'+nodeId+'">show</a>'
				kwic += ' <a href="'+localLink+'">';
				if (fragTitle) {
				  kwic += fragTitle;
				} else {
				  kwic += 'show';
				}
				kwic += '</a>';
				kwic += '</li>';
			});
			
			var kwicbox = '';
			if(kwic != '') {
				//kwicbox =  '<div class="expendable">KWIC';
				kwicbox = '<ol class="resultset">' +  kwic + '</ol></div>';
			} 
						
			var pathResult = '';
			if(path.length > 0) {
				pathResult = '<dl class="meta"><dt>Path:</dt><dd>'+path+'</dd></dl>';
			}
			
			var sendToLink = '';
			var basketStateId = 'basketToggleId';
			var basketState = '';
			var downloadLink = '';
			var size = '';
			
			if (format == 'text/xml') {
			    sendToLink = '<dd>'+sendToVoyantLinkHtml(uri)+'</dd>';;
			    basketState = '<input type="checkbox" id="'+basketStateId+'"/>';
			} else if (format == 'text/plain') {
			    basketState = '<input type="checkbox" id="'+basketStateId+'"/>';
			}
			
			if(format.search('aggregation') == -1) {
			    //var download = 'http://textgrid-ws3.sub.uni-goettingen.de/tgcrud/rest/'+uri+'/data';
			    var download = tgcrudURL + 'rest/'+uri+'/data';
			    // REC: Voyant must be able to reach the data the same way the browser is. If that
			    // does not work, e.g. because we still have to use SSH forwarding to access the 
			    // server, we need to provide Voyant with a different URL.
			    //var voyantDownload = "http://localhost/services/tgcrud-public/" + 'rest/'+uri+'/data'
			    downloadLink = '<dd><a href="' + download + '">download</a></dd>';
			    size = '<dt>Size:</dt><dd>'+$.CalcSize(extent)+'</dd>';
			}
			
			var element = $('#searchresults').append(   '<li><h4>'+basketState+'<a href="'+mainLink+'">'+title+'</a></h4>'
										+ '<dl class="meta">'
										+ '<dt>Format:</dt><dd>'+format+'</dd>'
//										+ '<dt>Published:</dt><dd>'+formatDate(date)+'</dd>'
										+ size
										+ downloadLink
										+ sendToLink
//										+ '<dt>Persistent Identifier</dt><dd>'+pid+'</dd>'
										+ '</dl>'
										+ pathResult
										+ kwicbox
										+ '</li>');
			
			if (format == 'text/xml' || format == 'text/plain') {
				var basketInput = element.find('#'+basketStateId)[0];
				basketInput.checked = basketContains(uri);
				basketInput.removeAttribute('id');
				basketInput.onclick = function() {
					basketToggle(uri, title);
					this.checked = basketContains(uri);
				};
			}
		});
		
		return resultNodes.size();
}

/**
 *TODO: remove code duplication (s.o.)
 */
 /*
function showPrefixedHits(xml) {

		var xsltUrl = 'http://textgrid-ws3.sub.uni-goettingen.de/exist-public/rest/xslt/transform.xql';
		
		var resultNodes = $(xml).find('*|result');
			
		$(resultNodes).each(function(){
			
			//window.log(this);
			
			var title = $(this).find('*|title').text();
			var format = $(this).find('*|format').text();
			var uri = $(this).find('textgridUri').text();
			var date = $(this).find('issued').text();
			var pid = $(this).find('pid').text();
			var extent = $(this).find('extent').text();
			var size = $.CalcSize(extent);
			
			var path = showPath($(this));

			var kwic = '';
			$(this).find('[nodeName="tgs:kwic"]').each(function() {
				kwic += '<li>';
				kwic += $(this).find('[nodeName="tgs:left"]').text();
				kwic += '<strong class="red">' + $(this).find('[nodeName="tgs:match"]').text() + '</strong>';
				kwic += $(this).find('[nodeName="tgs:right"]').text();
				kwic += '</li>';
			});
			
			var kwicbox = '';
			if(kwic != '') {
				//kwicbox =  '<div class="expendable">KWIC';
				kwicbox = '<ol class="resultset">' +  kwic + '</ol></div>';
			} 
			
            var download = 'http://textgrid-ws3.sub.uni-goettingen.de/tgcrud/rest/'+uri+'/data';
            var mainLink = '';
			if(format=='text/xml') {
               var id = uri.substr(9);
               //other = '<dt>view:</dt><dd><a href="'+xsltUrl+'?id='+id+'">html</a>'
                //    + '&nbsp;<a href="'+xsltUrl+'?id='+id+'&sheet=fo2">fo</a></dd>';
               //var mainLink = xsltUrl+'?id='+id;
               mainLink = 'browse.html?id='+uri;
            } else if(format.search('aggregation') != -1) {
				mainLink = 'browse.html?id='+uri;
			}else if(format=='text/tg.work+xml') {
				mainLink = 'browse.html?id='+uri;
			} else if(format.search('application/x-shockwave-flash') != -1) {
				mainLink = 'browse.html?id='+uri;
			} else {
				mainLink='http://textgrid-ws3.sub.uni-goettingen.de/tgcrud/rest/'+uri+'/data';
			}
			
			var pathResult = '';
			if(path.length > 0) {
				pathResult = '<dl class="meta"><dt>path:</dt><dd>'+path+'</dd></dl>';
			}
			
			$('#searchresults').append(   '<li><h4><a href="'+mainLink+'">'+title+'</a></h4>'
										+ '<dl class="meta">'
										+ '<dt>Format:</dt><dd>'+format+'</dd>'
										+ '<dt>Published:</dt><dd>'+formatDate(date)+'</dd>'
										+ '<dt>Size:</dt><dd>'+size+'</dd>'
										+ '<dt>Download:</dt><dd><a href="' + download + '">plain</a>'
//										+ '<dt>Persistent Identifier</dt><dd>'+pid+'</dd>'
										+ '</dl>'
										+ pathResult
										+ kwicbox
										+ '</li>');
		});
		
		return resultNodes.size();

}
*/

function showPath(xml) {
	
	var pathres='';
	
	$(xml).find('TGS|pathGroup').each(function() {
		//window.log(this);
		$(this).find('TGS|path').each(function() {
			var path = '';
			$(this).find('TGS|entry').each(function() {
				var title = $(this).find('TGS|title').text();
				var uri = $(this).find('TGS|textgridUri').text();
				var format = $(this).find('TGS|format').text();
				
				if(format.search('aggregation') != -1) {
					link = 'browse.html?id='+uri;
				} else {
					//var link = "http://textgrid-ws3.sub.uni-goettingen.de/tgcrud/rest/"+uri+"/metadata";
					var link = tgcrudURL + 'rest/'+uri+'/metadata';
				}
			
				/*
				if(format.search('edition') != -1) {
					icon = 'edition.gif'
				} else if(format.search('collection') != -1) {
					icon = 'collection.gif'
				} else if (format.search('aggregation') != -1){
					icon = 'aggregation.gif'
				}
			
				path += '<img src="images/'+icon+'"/><a href="'+link+'">' + title + '</a> &gt; ';
				*/
				path += '<a href="'+link+'">' + title + '</a> &gt; ';
				
			});	
			path = path.substring(0, path.length-6);
			//var link = 'http://textgrid-ws3.sub.uni-goettingen.de/tgcrud/rest/?id='+uri+'/metadata';
			//path += '<a href="'+link+'">' + title + '</a>';
			
			pathres += path;
			
		});
	});
	
	//var pathres = '<dl class="meta"><dt>path:</dt><dd>'+path+'</dd></dl>';
	return pathres
}

(function($) {
	/* thanks to http://blog.jbstrickler.com/ */
	$.CalcSize = (function(size) {
	    var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"],
	        tier = 0;
	 
	    while(size >= 1024) {
	        size = size / 1024;
	        tier++;
	    }
	 
	    return Math.round(size * 10) / 10 + " " + suffix[tier];		
	})
})(jQuery);

function formatDate(dateString) {
	
	var date = parseXSDDateString(dateString);
	//return date.toLocaleString();
	var res = $.format.date(date, "dd/MM/yyyy HH:mm:ss")
	//return date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear();
	return res;
}

/* http://www.experts-exchange.com/Programming/Languages/Scripting/JavaScript/Q_21889287.html */
function parseXSDDateString(dateString) {

  /*textgrid dates may contain miliseconds */
  if(dateString.indexOf('.') > 0 )	{
	dateString = dateString.substr(0, dateString.indexOf('.')) + dateString.substr(dateString.indexOf('+'));
  }

  var Zp=(dateString.charAt(10)=="T")?19:10;
  var xDate = new Date(dateString.substr(0,Zp).replace(/-/g,'/').replace("T"," "));
  if(dateString.length>Zp){
    xDate.setMinutes(xDate.getMinutes()+xDate.getTimezoneOffset());
    if(dateString.charAt(Zp)!="Z"){
      var tZ = dateString.substr(Zp).split(":");
      tZ=tZ[0]*60+(tZ[1]*1);
      xDate.setMinutes(xDate.getMinutes()+tZ);
    }
  }
  return xDate;
}

function tgcrudDataLink(id) {
//	return 'http://textgrid-ws3.sub.uni-goettingen.de/tgcrud/rest/'+id+'/data';
	return tgcrudURL + 'rest/'+id+'/data';
}

function tgcrudMetaDataLink(id) {
//	return 'http://textgrid-ws3.sub.uni-goettingen.de/tgcrud/rest/'+id+'/metadata';
	return tgcrudURL + 'rest/'+id+'/metadata';
}

// this should really be integrated in jQuery. 
// Below code is derived (i.e. copied and modified) from jquery.xmlns.js
function attrByNamespace(elem, attrName, ns){
    //  Some common default namespaces, that are treated specially by browsers.
    //
    var default_xmlns = {
        "xml": "http://www.w3.org/XML/1998/namespace",
        "xmlns": "http://www.w3.org/2000/xmlns/",
        "html": "http://www.w3.org/1999/xhtml/"
    };
    if(ns == "") {
        return elem.getAttribute(attrName);
    }
    //  Directly use getAttributeNS if applicable and available
    if(typeof elem.getAttributeNS != "undefined") {
        return elem.getAttributeNS(ns,attrName);
    }
    //  Need to iterate over all attributes, either because we couldn't
    //  look it up or because we need to match all namespaces.
    var attrs = elem.attributes;
    for(var i=0; attrs[i]; i++) {
        var ln = attrs[i].localName;
        if(!ln) {
            ln = attrs[i].nodeName
            var idx = ln.indexOf(":");
            if(idx >= 0) {
                ln = ln.substr(idx+1);
            }
        }
        if(ln == attrName) {
            if(attrs[i].namespaceURI == ns) {
                return attrs[i].nodeValue;
            }
            if(attrs[i].namespaceURI === "" && attrs[i].prefix) {
                if(attrs[i].prefix == default_xmlns_rev[ns]) {
                    return attrs[i].nodeValue;
                }
            }
        }
    }
}

/*
function getMetadata(id) {
	return tgsearchInfoRequest(id, false);
}

function getMetadataAndPath(id) {
	return tgsearchInfoRequest(id, true);
}

function tgsearchInfoRequest(id, path) {
	window.log('ireq called');
	var infoRequest = 'http://'+window.location.hostname+'/tgsearch-public/info/'+id+'?path='+path;
	
	$.ajax({
	  url: infoRequest,
	  dataType: 'xml',
	  success: function(xml){
		  window.log('done');
		  respElem = $(xml).find('[nodeName="tgs:response"]');
		  window.log(respElem);
		  return xml;
	  }
	});
}
*/
//function log(input) {
//	window.log(input);
//}
